#!/usr/bin/python3
import sys, random, shelve
from parse import parse

def parseSrt(f):
    wait = "id"
    rval = []
    txt = ""
    for l in f:
        if wait == "id":
            if len(l) <= 1: continue
            no = int(l)
            wait = "times"
        elif wait == "times":
            #print(l)
            r = parse('{fh:d}:{fm:d}:{fs:d},{fu:d} -->'
                    '{th:d}:{tm:d}:{ts:d},{tu:d}', l)
            f = ((r['fh'] * 60 + r['fm']) * 60 + r['fs']) * 1000 + r['fu']
            t = ((r['th'] * 60 + r['tm']) * 60 + r['ts']) * 1000 + r['tu']
            assert f < t
            wait = "text"
        elif wait == "text":
            if len(l) >= 2:
                txt += l
            else:
                rval.append((no,f,t,txt[:-1]))
                wait = "id"
                txt = ""
    return rval

def getPairs():
    native = parseSrt(open(sys.argv[1]))
    native.sort()
    learn = parseSrt(open(sys.argv[2]))
    learn.sort()

    p = []

    while len(native) * len(learn):
        nn, nf, nt, nx = native[0]
        ln, lf, lt, lx = learn[0]
        if nt < lf:
            native.pop(0)
        elif nf > lt:
            learn.pop(0)
        else:
            p.append((nx, lx))
            native.pop(0)
            learn.pop(0)
    return p

def getNums(p):
    nums = list(range(len(p)))
    random.shuffle(nums)
    return nums

if len(sys.argv) == 4:
    p = getPairs()
    nums = getNums(p)
    j = 0
    okay = 0
    oops = 0
    d = shelve.open(sys.argv[3])
    d["p"] = p
elif len(sys.argv) == 2:
    d = shelve.open(sys.argv[1])
    p = d["p"]
    nums = d["nums"]
    j = d["j"]
    okay = d["okay"]
    oops = d["oops"]
else: assert False

def replace(msg):
    return msg.replace("\n-", "\n   -").replace("\n", " ")

while j < len(p):
    d["j"] = j
    d["nums"] = nums
    d["okay"] = okay
    d["oops"] = oops
    d.sync()
    n = nums[j]
    s = random.sample(p, 26)
    if p[n] in s:
        i = s.index(p[n])
    else:
        i = random.randrange(26)
        s[i] = p[n]
    print("Сообщение {} из {}:".format(j, len(p)))
    print(" ", replace(p[n][0]))
    print("Варианты перевода:")
    for k in range(26):
        print(chr(k + ord('a')), replace(s[k][1]))
    c = -1
    while c not in range(26):
        print("Выберите вариант перевода> ", end="")
        sys.stdout.flush()
        c = ord(sys.stdin.readline()[0]) - ord('a')
    if c == i:
        okay += 1
        print("\n*** *** *** Правильно! Вы угадали {} переводов, сделав"
            " {} ошибок. *** *** ***".format(okay, oops))
        j += 1
    else:
        oops += 1
        print("\n~~~ ~~~ ~~~ Не правильно. Вы угадали {} переводов, сделав"
            " {} ошибок.~~~ ~~~ ~~~".format(okay, oops))
        print(" ", p[n][0].replace("\n", "\t"))
        print("переводится как")
        print(chr(i + ord('a')), s[i][1].replace("\n", "\t"))
        n = nums.pop(j)
        nums.append(n)

