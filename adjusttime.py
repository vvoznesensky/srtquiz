#! /usr/bin/python3

import sys, re

TS = ("([0123456789][0123456789]?):"
    "([0123456789][0123456789]?):"
    "([0123456789][0123456789]?),"
    "([0123456789][0123456789]?[0123456789]?)")

tr = re.compile(TS)
cr = re.compile(TS + " --> " + TS + "\r\n")

DELTA = int(sys.argv[1])
ES = tr.fullmatch(sys.argv[2])
ER = tr.fullmatch(sys.argv[3])

def nums_to_usecs(h, m, s, u):
    return ((int(h)*60+int(m))*60+int(s))*1000+int(u)

def usecs_to_nums(us):
        us = 0 if us < 0 else us
        u = us % 1000
        us //= 1000
        s = us % 60
        us //= 60
        m = us % 60
        h = us // 60
        return (h, m, s, u)

k = nums_to_usecs(ER[1], ER[2], ER[3], ER[4]) / nums_to_usecs(
        ES[1], ES[2], ES[3], ES[4])

for l in sys.stdin:
    ms = cr.fullmatch(l)
    if ms is None:
        print(l, end="")
    else:
        fr=nums_to_usecs(ms[1], ms[2], ms[3], ms[4])
        fr = int(fr * k)
        fr += DELTA
        fh, fm, fs, fu = usecs_to_nums(fr)
        to=nums_to_usecs(ms[5], ms[6], ms[7], ms[8])
        to = int(to * k)
        to += DELTA
        th, tm, ts, tu = usecs_to_nums(to)
        print(f"{fh:02d}:{fm:02d}:{fs:02d},{fu:03d} --> "
                f"{th:02d}:{tm:02d}:{ts:02d},{tu:03d}\r\n", end="")

